(function($) {

Drupal.behaviors.ml_cookies = {
  attach: function (context, settings) {
    $('.CookieOptions-continue').click(function(event) {
      event.preventDefault();
      $('.Container--cookies').hide();
    });
  }
}

})(jQuery);
